class Unrar ():
	
	def __init__(self, dir_files):
		self.dir_files = dir_files

	# get the rar file(s) and return list of file(s)
	def rar_files(self):
		actual_rars = []
		for item in self.dir_files:
			if item[-4:] == '.rar':
				actual_rars.append(item)
		return actual_rars
